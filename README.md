Lorys, Angélique, Yanis

# package

This package aims at building a shiny web application that provides
results based on our work over a data set. Once the application is
built, the user can look at the different analysis we did. Our work can
be summed up in two different parts, the univariate and bivariate
analysis

## Instalation

You can install our package like so:

``` r
devtools::install_gitlab("LorysD/M1-S1-R-project")
```

## Usage

Once the app is installed, you can build the app by launching the
following R commands:

``` r
library(WOA)
build_app()
```

## Report and presentation

There are four extra files to the ones you usually find in a R package:

-   Our report of this analysis as a RMarkdown file:
    **RapportJOrmk.Rmd**
-   A beamer presentation can be obtained by knitting the file:
    **Diapo.Rmd**
-   The pdf version of our report : **RapportJOrmk.pdf**
-   The pdf version of our presentation: **Diapo.pdf**

If you want to compile both rmarkdown files by yourself you need to
clone this repository like so:

``` bash
git clone https://gitlab.com/LorysD/m1-s1-r-project.git
```

Once the repository is cloned, you need to run, in your local git
repository, the following command:

``` r
rmarkdown::render("RapportJOrmk.Rmd")
rmarkdown::render("Diapo.pdf")
```
