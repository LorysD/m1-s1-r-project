#include <Rcpp.h>
using namespace Rcpp;

#ifndef SPORT_INDEX
#define SPORT_INDEX 2
#endif

#ifndef COUNTRY_INDEX
#define COUNTRY_INDEX 5
#endif

// [[Rcpp::export]]
int get_index(Rcpp::CharacterVector vec,Rcpp::String chaine){

  int vector_len=vec.length();
  int index;
  for (int i=0; i< vector_len; i++){
    if(vec[i]==chaine){
      index=i;
      break;
    }
  }
  return index;
}

// [[Rcpp::export]]
int get_max_index(Rcpp::IntegerVector vec){
  int curr=0;
  int len=vec.length();
  for(int i=0;i<len;i++){
    if(vec[i]>vec[curr]){
      curr=i;
    }
  }
  curr++;
  return curr;
}

// [[Rcpp::export]]
Rcpp::List Rcpp_get_best_country_sport(Rcpp::StringVector sports,Rcpp::StringVector countries,Rcpp::StringVector unique_countries,Rcpp::String sport){

  if(sports.length()!=countries.length()){
    std::cerr<<"Error: sports and countries must be the same length" << std::endl;
  }

  Rcpp::IntegerVector number_medals(unique_countries.length(),0);
  int medals=0;
  Rcpp::String best_country;

  int len=sports.length();
  int index;

  for (int i=0; i<len;i++){
    if (sports[i]==sport){
      index=std::distance(unique_countries.begin(),std::find(unique_countries.begin(),unique_countries.end(),countries[i]));
      number_medals[index]=number_medals[index]+1;
    }
  }
  index=get_max_index(number_medals);
  medals=medals+number_medals[index-1];
  best_country=best_country+unique_countries[index-1];

  Rcpp::List ret= Rcpp::List::create(best_country,medals);
  return ret;
}


// [[Rcpp::export]]
Rcpp::DataFrame Rcpp_get_best_country(Rcpp::StringVector sports, Rcpp::StringVector countries,Rcpp::StringVector unique_sports, Rcpp::StringVector unique_countries){

  if(sports.length()!=countries.length()){
    std::cerr << "Err: Sports and countries must be the same length" << std::endl;
  }

  Rcpp::DataFrame df;
  Rcpp::IntegerVector nb_medals(unique_sports.length(),0);
  Rcpp::StringVector best_countries(unique_sports.length(),"");
  int sp_len=unique_sports.length();
  Rcpp::List my_list;
  const char* prem;

  for(int i=0;i<sp_len;i++){
    my_list=Rcpp_get_best_country_sport(sports,countries,unique_countries,(Rcpp::String) unique_sports[i]);
    nb_medals[i]=my_list[1];
    prem = CHAR((STRING_ELT(my_list[0],0)));
    best_countries(i)=prem;
  }

  df=Rcpp::DataFrame::create(Rcpp::Named("Sports")=unique_sports,Rcpp::Named("Countries")=best_countries,Rcpp::Named("Nb_Medals")=nb_medals);

  return df;
}
