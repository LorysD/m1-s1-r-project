#' builds the user_inteerface for the shiny web app
#'
#' @return nothing
#' @export
#'

build_ui <- function(){
  header <- dashboardHeader(title="WOA")

  sidebar <- dashboardSidebar(
    sidebarMenu(id="tabs",
                menuItem("Intro",tabName = "Intro"),
                menuItem("Univariate",
                         menuSubItem("Number of medals",tabName="medals"),
                         menuSubItem("Host proportion",tabName="hostproportion")),
                menuItem("Bivariate",
                         menuSubItem("Medals-Countries",tabName="Medals-and-Countries"),
                         menuSubItem("Medals-Continents",tabName="Medals-and-Continents"),
                         menuSubItem("Medals-Ranking",tabName="Ranking-of-medals"),
                         menuSubItem("Host-medals-year",tabName="hostmedalsyear"),
                         menuSubItem("Sports-Host",tabName="Sports-and-Host"),
                         menuSubItem("Medals-Year-Country",tabName="Medals-of-country-and-year"),
                         menuSubItem("Sports-Countries",tabName="Sports-and-Countries")),
                menuItem("Conclusion",tabName="Conc"),
                menuItem("Links",tabName="Links")
  )
  )


  body <- dashboardBody(

    tags$head(
      includeCSS(system.file("www/custom.css",package="WOA"))
    ),
    tabItems(
      tabItem(
        tabName="Intro",
        h1("An analysis of winter Olympic Games",align="center"),
        fluidRow(
          column(8,offset=4,tag("div",list(
            class="logo-style",
            imageOutput("photo"))
          )
          )),
        fluidRow(
          column(8,offset=2,
                 tag("div",list(
                   align="justify",
                    class="intro-style",
                    HTML("<p>Hello and welcome to our shiny web application. This web app shows informations about the dataset donneesjo.
                    This data set provides information about winter olympic games from 1924 to 2010. There are two main parts
                    in our analysis. First, in the 'univariate' tab, you will find a pie chart of the different type of medals
                    while the second tab gives information about the number of medals won by host each year.<p/>

                    <p>The second part of our analysis can be found in the 'bivariate' tab where we compare some variables.
                    In the first sub-tab, you will be able to compare the countries of your choice in terms of the number of medals
                    won whilst the second sub-tab compares continent to one another.<p/>

                    <p>A more precise analysis can be found in the sub-tab \"Medals-Ranking\" where you can get the podium for
                    each year and the number of countries to compare.
                    The next sub-tab, \"Sports-Host\", represents the frequency of host per sport that is the the number
                    of times each sport was the one in wihch the host country had performed greatly.<p/>

                    <p>You can get more information for a given country in the sub-tab \"Medals-Year-Country\" with a plot
                    of the number of medals won by the country of your choice over the years.
                    The scatterplot, along with the prediction interval, can also be obtained by ticking the box \"Régression
                    linéaire\"<p/>

                    <p>Finally, the sub-tab \"Sports-countries\", allows you to compare the countries that you want int the
                    sport of your choice. A plot of the best countries in the different sports can be seen by clicking
                    on the panel \"Best performances in different sports\"<p/>")
        )))
        )),
      tabItem(
        tabName="medals",
        fluidRow(
          h1("Plot of the medals",align="center"),
          column(1),
          column(10,plotOutput("plotmedals",height="700px"),dataTableOutput('tablemed'),textOutput("textemed"))
        )
      ),
      tabItem(
        tabName="hostproportion",
        fluidRow(
          h1("Plot of the host proportion",align="center"),
          column(1),
          column(10,plotOutput("plothost",height="800px"),textOutput("texthost"))
        )
      ),
      tabItem(
        tabName="hostmedalsyear",
        fluidRow(
          h1("Host and JO medals throughout the years",align="center"),
          column(1),
          column(10,plotOutput("plot1",height="800px"))
        )
      ),
      tabItem(
        tabName="Medals-and-Continents",
        fluidRow(
          h1("Continents",align="center"),
          column(1),
          column(10,plotOutput("plot2",height="800px"))
        )
      ),
      tabItem(
        tabName="Ranking-of-medals",
        fluidRow(
          h1("Podium",align="center"),
            numericInput('nbpays', "Choose the number of contries in the podium", value = 3, min = 1, max = length(unique(countries)), step = 1),
            selectInput("year","Choose the year",sort(unique(donneesjo$year))),
            selectInput("couleur","Choose the type of medals",c("Total","Gold","Silver","Bronze")),
          column(1),
          column(10,plotOutput("plotpodium",height="800px"))
          )
      ),
      tabItem(
        tabName="Medals-of-country-and-year",
        h1("Medals won per year for a given country",align="center"),
        fluidRow(
          selectInput("pays","Country",unique(donneesjo$pays)),
          checkboxInput("lm","Linear model"),
          column(1),
          column(10,plotOutput("plotY",height="800px"),textOutput("textY"))
        )),

      tabItem(
        tabName="Medals-and-Countries",
        h1("Countries",align="center"),
        fluidRow(
        tag("div",list(
          class="medals-and-countries-left",
          checkboxGroupInput("countries","Your choice",countries))),
        tag("div",list(
          class="medals-and-countries-right",
          plotOutput("plot3",height="800px")
        )
        ))),
      tabItem(
        tabName="Sports-and-Countries",
          navbarPage("",id="myNavBar",inverse = TRUE,
                     tabPanel("countries comparison in the different sports",
                              h1("Sport-countries",align="center"),
                              tag("div",list(
                                class="Sports-and-Countries-navbar-style",
                                checkboxGroupInput("countries2","Your choice",countries),
                                radioButtons("sports","Your choice",sports)
                              )),
                              tag("div",list(
                                class="Sports-and-Countries-graph",
                                plotOutput("plot4",height="700px")
                              ))),
                     tabPanel("Best performances in the different sports",
                              fluidRow(
                                h1("Best countries in the different sports",align="center"),
                                plotOutput("plot5",height="600px")
                              ))
          )
      ),
      tabItem(
        tabName="Sports-and-Host",
        fluidRow(
          h1("Plot of the host according to the Sports",align="center"),
          column(1),
          column(10,plotOutput("plotHS",height="800px"),textOutput("textHS"))
        )),
        tabItem(
          tabName="Conc",
          h1("Conclusion",align="center"),
          fluidRow(
            column(8,offset=2,
            tag("div",list(
              align="justify",
              class="conclusion-style",
              HTML("<p>To conclude, with this application we can learn more about WOA.<p/>
              <p>There is the same number of medail color. You can find  which contry or continent win the most.
              We have crossed interesting variables to understand informations like the most medals won for a specifique sport,
              year and country or if there is some correlation between them and which.<p/>
              <p>By searching you will be able to deduce and find the information you want<p/>")
            ))
          ))
        ),
      tabItem(
        tabName="Links",
        fluidRow(
          h1("Links for our project",align="center"),
          htmlOutput("source"),
          HTML(("<p class='my_link'>
    The analysis was performed on a database that gathers medals for olympic games from 1924 to 2010. <br/>
    The database can be found on the <a href='https://www.data.gouv.fr/fr/datasets/winter-olympics-medals/'>data.gouv website</a><br/>
    It is precised that those data were extracted from the International Olympic Comittee in February 2014.<br/>
    We built and S3 R class called <mark> continent </mark> using a <a href='https://www.business-plan-excel.fr/liste-pays-du-monde-excel-capitale-continent'>dataset</a> about countries and the continent they belng to<br/>
    This class also uses informations about continents that were extracted from <a href='https://fr.wikipedia.org/wiki/Continent'>wikipedia<a/>
          </p>"))
        )
      )
    )
  )


  ui <- dashboardPage(
    skin="green",
    header,
    sidebar,
    body
  )

  return(ui)
}




#' builds the server for the shiny web app
#'
#' @param input input of the session
#' @param output output of the session
#' @param session session
#'
#' @return nothing
#' @export
server <- function(input,output,session){

  output$plotmedals <- renderPlot({
    c<-verifcouleur()
    c$Plot
  })
  output$tablemed<-renderDataTable({
    c<-verifcouleur()
    c$Tab
  })
  output$textemed<-renderText({
    c<-verifcouleur()
    c$Texte
  })
  output$plothost <- renderPlot({
    h<-host()
    h$Plot
  })
  output$texthost<-renderText({
    h<-host()
    h$Texte
  })
  output$plot1 <- renderPlot({
    plot(host_win_per_year())
  })
  output$plot2 <- renderPlot({
    plot(compare_continents_medals())
  })

  output$plotpodium <- renderPlot({
    plot(podium(input$year,input$nbpays,input$couleur))
  })
  output$plotY <- renderPlot({
    plot(medTotAnnPD(input$pays,input$lm))

  })
  output$textY <- renderText({
    pvalue_lm(input$pays,input$lm)
  })
  output$plot3 <- renderPlot({
    plot(plot_countries_medals(input$countries))
  })
  output$plot4 <- renderPlot({
    plot(count_medals_countries(input$sports,input$countries2))
  })
  output$plot5 <- renderPlot({
    plot(Rcpp_countries_performance_in_sports())
  })
  output$plotHS <- renderPlot({
    p<-sport_host()
    plot(p$Plot)
  })
  output$textHS<-renderText({
    t<-sport_host()
    t$Texte
  })
  output$photo <- renderImage({
    list(
      src=system.file("extdata/Olympic_rings_without_rims.png",package="WOA"),
      #src=file.path("Image/Olympic_rings_without_rims.png"),
      width=400,
      height=200
    )
  },deleteFile = FALSE)
}


#' launches the Shiny web app
#'
#'
#' @return nothing
#' @export
#'
#'@import devtools dplyr readr ggplot2 scales forcats shiny shinydashboard htmltools shinyWidgets foreach Rcpp
build_app <- function(){

  data("donneesjo")
  data("continent_countries_name_english")
  data("population_area_continent")
  data("tabDesMedai")
  data("conts_name")
  data("sports")
  data("countries")
  change_countries(countries)

  user_interface <- build_ui()
  shinyApp(user_interface,server)
}





